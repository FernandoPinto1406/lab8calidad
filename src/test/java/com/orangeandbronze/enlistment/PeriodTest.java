package com.orangeandbronze.enlistment;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.orangeandbronze.enlistment.exceptions.RoomConflictException;
import com.orangeandbronze.enlistment.exceptions.ScheduleConflictException;
import org.junit.Test;

public class PeriodTest {

    @Test
    public void check_valid_periods() {
        //Given 4 valid periods
        //When they are initialized
        //No exception is thrown
        assertDoesNotThrow(() -> {
            Period period1 = new Period(830, 900);
            Period period2 = new Period(900, 1200);
            Period period3 = new Period(1430, 1630);
            Period period4 = new Period(900, 1030);
        });
    }

    @Test
    public void check_invalid_periods() {
        assertThrows(IllegalArgumentException.class, () -> {
            //Given 4 periods that are invalid: invalid start time, invalid end time, valid increment, invalid period
            Period period1 = new Period(815, 1030);
            Period period2 = new Period(1000, 1930);
            Period period3 = new Period(1230, 1250);
            Period period4 = new Period(1230, 1215);
        });
    }

    @Test
    public void testCheckOverlaps_NoOverlap() {
        // Given two schedules with non-overlapping periods
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 900));
        Schedule schedule2 = new Schedule(Days.TF, new Period(930, 1000));

        assertDoesNotThrow(() -> schedule1.checkOverlaps(schedule2));
    }

    @Test
    public void testCheckOverlaps_ThrowsScheduleConflictException() {
        Period period1 = new Period(830, 930);
        Period period2 = new Period(900, 1000);
        assertThrows(ScheduleConflictException.class, () -> period1.checkOverlaps(period2));
    }

    @Test
    public void testCheckOverlaps_NoExceptionThrown() {
        Period period1 = new Period(830, 930);
        Period period2 = new Period(1000, 1100);
        assertDoesNotThrow(() -> period1.checkOverlaps(period2));
    }

    @Test
    public void testPeriodConstructor_StartTimeNotIn30Or00_ThrowsException() {
        // Arrange
        int startTime = 840; // Not ending in 00 or 30

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Period(startTime, 900);
        });
    }

    @Test
    public void testPeriodConstructor_EndTimeNotIn30Or00_ThrowsException() {
        // Arrange
        int endTime = 825; // Not ending in 00 or 30

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Period(800, endTime);
        });
    }

    @Test
    public void testPeriodConstructor_EndTimeBeforeStartTime_ThrowsException() {
        // Arrange
        int startTime = 900;
        int endTime = 830; // End time before start time

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Period(startTime, endTime);
        });
    }

    @Test
    public void testConstructor_ValidTimeRange_NoExceptionThrown() {
        // Arrange
        int startTime = 900; // 9:00 AM
        int endTime = 1500;  // 3:00 PM

        // Act & Assert
        assertDoesNotThrow(() -> new Period(startTime, endTime));
    }

    @Test
    public void testConstructor_InvalidStartTime_OutsideRange_ThrowsException() {
        // Arrange
        int startTime = 800; // 8:00 AM
        int endTime = 1500;  // 3:00 PM

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Period(startTime, endTime));
    }

    @Test
    public void testConstructor_InvalidEndTime_OutsideRange_ThrowsException() {
        // Arrange
        int startTime = 900; // 9:00 AM
        int endTime = 1800;  // 6:00 PM

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Period(startTime, endTime));
    }

    @Test
    public void testConstructor_InvalidTimeRange_StartAfterEnd_ThrowsException() {
        // Arrange
        int startTime = 1600; // 4:00 PM
        int endTime = 1500;   // 3:00 PM

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> new Period(startTime, endTime));
    }
}
