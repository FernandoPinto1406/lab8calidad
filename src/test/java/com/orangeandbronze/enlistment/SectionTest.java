package com.orangeandbronze.enlistment;

import com.orangeandbronze.enlistment.exceptions.RoomConflictException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SectionTest {
    @Test
    public void testCheckRoomConflict_NoConflict() {
        Subject subject1 = new Subject("Math", 115);
        Subject subject2 = new Subject("Physics", 935);

        Section section1 = new Section("A", new Schedule(Days.MTH, new Period(830, 1000)), new Room("A1", 2), subject1);
        Section section2 = new Section("B", new Schedule(Days.MTH, new Period(1030, 1200)), new Room("A2", 2), subject2);

        assertDoesNotThrow(() -> section1.checkRoomConflict(section2));
    }

    @Test
    public void testCheckRoomConflict_SameRoomAndOverlappingSchedule() {
        Subject subject1 = new Subject("Math", 115);
        Subject subject2 = new Subject("Physics", 935);

        Section section1 = new Section("A", new Schedule(Days.MTH, new Period(830, 1000)), new Room("A1", 2), subject1);
        Section section2 = new Section("B", new Schedule(Days.MTH, new Period(930, 1100)), new Room("A1", 2), subject2);

        RoomConflictException exception = assertThrows(RoomConflictException.class, () -> {
            section1.checkRoomConflict(section2);
        });

        assertEquals("Same room and schedule.", exception.getMessage());
    }

    @Test
    public void testCheckRoomConflict_SameRoomAndNonOverlappingSchedule() {
        Subject subject1 = new Subject("Math", 115);
        Subject subject2 = new Subject("Physics", 935);

        Section section1 = new Section("A", new Schedule(Days.MTH, new Period(830, 1000)), new Room("A1", 2), subject1);
        Section section2 = new Section("B", new Schedule(Days.MTH, new Period(1100, 1200)), new Room("A1", 2), subject2);

        assertDoesNotThrow(() -> section1.checkRoomConflict(section2));
    }

    @Test
    public void testCheckRoomConflict_NoRoomConflict_NoExceptionThrown() {
        Room room1 = new Room("RoomA", 30);
        Room room2 = new Room("RoomB", 30);
        Period period1 = new Period(900, 1000);
        Period period2 = new Period(1000, 1100);
        Schedule schedule1 = new Schedule(Days.MTH, period1);
        Schedule schedule2 = new Schedule(Days.TF, period2);
        Subject subject1 = new Subject("Math", 3, false);
        Subject subject2 = new Subject("English", 3, false);
        Section section1 = new Section("SecA", schedule1, room1, subject1);
        Section section2 = new Section("SecB", schedule2, room2, subject2);

        assertDoesNotThrow(() -> {
            section1.checkRoomConflict(section2);
        });
    }

    @Test
    public void testCheckRoomConflict_RoomConflict_ThrowsRoomConflictException() {
        Room room = new Room("RoomA", 30);
        Period period1 = new Period(900, 1000);
        Period period2 = new Period(1000, 1100);
        Schedule schedule1 = new Schedule(Days.MTH, period1);
        Schedule schedule2 = new Schedule(Days.TF, period2);
        Subject subject1 = new Subject("Math", 3, false);
        Subject subject2 = new Subject("English", 3, false);
        Section section1 = new Section("SecA", schedule1, room, subject1);
        Section section2 = new Section("SecB", schedule2, room, subject2);

        assertThrows(RoomConflictException.class, () -> {
            section1.checkRoomConflict(section2);
        });
    }

    @Test
    public void testCheckRoomConflict_SameRoomNoConflict_NoExceptionThrown() {
        Room room = new Room("RoomA", 30);
        Period period1 = new Period(900, 1000);
        Period period2 = new Period(1000, 1100);
        Schedule schedule1 = new Schedule(Days.MTH, period1);
        Schedule schedule2 = new Schedule(Days.TF, period2);
        Subject subject1 = new Subject("Math", 3, false);
        Subject subject2 = new Subject("English", 3, false);
        Section section1 = new Section("SecA", schedule1, room, subject1);
        Section section2 = new Section("SecB", schedule2, room, subject2);

        assertDoesNotThrow(() -> {
            section1.checkRoomConflict(section2);
        });
    }

    @Test
    public void testCheckRoomConflict_SameRoomWithConflict_ThrowsRoomConflictException() {
        Room room = new Room("RoomA", 30);
        Period period1 = new Period(900, 1000);
        Period period2 = new Period(950, 1050);
        Schedule schedule1 = new Schedule(Days.MTH, period1);
        Schedule schedule2 = new Schedule(Days.TF, period2);
        Subject subject1 = new Subject("Math", 3, false);
        Subject subject2 = new Subject("English", 3, false);
        Section section1 = new Section("SecA", schedule1, room, subject1);
        Section section2 = new Section("SecB", schedule2, room, subject2);

        assertThrows(RoomConflictException.class, () -> {
            section1.checkRoomConflict(section2);
        });
    }

    @Test
    public void testEquals_ObjectIsNull_ReturnsFalse() {
        Section section = new Section("SecA", null, null, null);

        boolean result = section.equals(null);

        assertFalse(result);
    }

    @Test
    public void testEquals_ObjectIsNotInstanceOfSection_ReturnsFalse() {
        Section section = new Section("SecA", null, null, null);

        boolean result = section.equals(new Object());

        assertFalse(result);
    }

    @Test
    public void testEquals_SameObjectInstance_ReturnsTrue() {
        Section section = new Section("SecA", null, null, null);

        boolean result = section.equals(section);

        assertTrue(result);
    }

    @Test
    public void testEquals_ObjectIsInstanceOfSectionButDifferentSectionID_ReturnsFalse() {
        Section section1 = new Section("SecA", null, null, null);
        Section section2 = new Section("SecB", null, null, null);

        boolean result = section1.equals(section2);

        assertFalse(result);
    }

    @Test
    public void testEquals_ObjectIsInstanceOfSectionAndSameSectionID_ReturnsTrue() {
        Section section1 = new Section("SecA", null, null, null);
        Section section2 = new Section("SecA", null, null, null);

        boolean result = section1.equals(section2);

        assertTrue(result);
    }
}
