package com.orangeandbronze.enlistment;

import com.orangeandbronze.enlistment.exceptions.RoomConflictException;
import org.junit.Test;


import static org.junit.Assert.*;

public class SubjectTest {
    @Test
    public void testEquals_WhenThisEqualsO() {
        Subject subject1 = new Subject("Math", 3);
        Subject subject2 = subject1;

        // Act
        boolean result = subject1.equals(subject2);

        // Assert
        assertTrue(result);
    }
    @Test
    public void testEquals_NullObject_ReturnsFalse() {
        Subject subject = new Subject("Math", 3);
        assertFalse(subject.equals(null));
    }

    @Test
    public void testEquals_DifferentClass_ReturnsFalse() {
        Subject subject = new Subject("Math", 3);
        assertFalse(subject.equals("Math"));
    }

    @Test
    public void testEquals_SameClass_ReturnsTrue() {
        Subject subject = new Subject("Math", 3);
        assertTrue(subject.equals(subject));
    }

    @Test
    public void testHashCode_NotNullSubjectID_ReturnsHashCode() {
        Subject subject1 = new Subject("Math", 3);
        Subject subject2 = new Subject("Math", 3);
        assertEquals(subject1.hashCode(), subject2.hashCode());
    }
    @Test
    public void testCheckRoomOverlaps_WhenOverlapOccurs_ThrowsRoomConflictException() {
        // Arrange
        Period period = new Period(900, 1000); // Período que se superpone

        // Crear instancias simuladas de las dependencias necesarias
        String roomName = "A101";
        Room room = new Room(roomName,115);
        Subject subject = new Subject("Math101", 3);

        // Crear una instancia de Schedule con las dependencias simuladas
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));

        // Crear una instancia de Section con las dependencias simuladas
        Section section = new Section("S101", schedule, room, subject);

        // Act
        try {
            schedule.getPeriod().checkRoomOverlaps(period, section);
            fail("Expected RoomConflictException was not thrown");
        } catch (RoomConflictException e) {
            // Assert
            assertEquals("Same room and schedule.", e.getMessage());
        }
    }
}
