package com.orangeandbronze.enlistment;

import com.orangeandbronze.enlistment.Room;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoomTest {

    @Test
    public void testHashCode() {
        Room room1 = new Room("A101", 30);
        Room room2 = new Room("A101", 30);
        assertEquals(room1.hashCode(), room2.hashCode());
    }

    @Test
    public void testEquals_FirstTwoConditionsTrue() {
        Room room1 = new Room("A101", 30);
        Room room2 = new Room("A101", 30);
        assertTrue(room1.equals(room2));
    }

    @Test
    public void testEquals_FirstConditionFalse() {
        Room room1 = new Room("A101", 30);
        Room room2 = new Room("B202", 30);
        assertFalse(room1.equals(room2));
    }

    @Test
    public void testEquals_SecondConditionFalse() {
        Room room1 = new Room("A101", 30);
        Room room2 = new Room("A101", 40);
        assertFalse(room1.equals(room2));
    }

    @Test
    public void testHashCode_2() {
        String roomName = "Room1";
        int capacity = 30;
        Room room1 = new Room(roomName, capacity);

        int hashCode1 = room1.hashCode();

        assertEquals(roomName.hashCode() ^ Integer.hashCode(capacity), hashCode1);
    }

    @Test
    public void testEquals_SameObject_ReturnsTrue() {
        Room room = new Room("Room1", 20);
        assertTrue(room.equals(room));
    }

    @Test
    public void testEquals_NullObject_ReturnsFalse() {
        Room room = new Room("Room1", 20);
        assertFalse(room.equals(null));
    }

    @Test
    public void testEquals_NonRoomObject_ReturnsFalse() {
        Room room = new Room("Room1", 20);
        assertFalse(room.equals("Not a Room object"));
    }

    @Test
    public void testEquals_SameCapacityAndRoomName_ReturnsTrue() {
        Room room1 = new Room("Room1", 20);
        Room room2 = new Room("Room1", 20);
        assertTrue(room1.equals(room2));
    }

    @Test
    public void testEquals_DifferentCapacity_ReturnsFalse() {
        Room room1 = new Room("Room1", 20);
        Room room2 = new Room("Room1", 30);
        assertFalse(room1.equals(room2));
    }

    @Test
    public void testEquals_DifferentRoomName_ReturnsFalse() {
        Room room1 = new Room("Room1", 20);
        Room room2 = new Room("Room2", 20);
        assertFalse(room1.equals(room2));
    }

    @Test
    public void testEquals_ObjectNotInstanceOfRoom_ReturnsFalse() {
        Room room = new Room("RoomA", 10);

        boolean result = room.equals(new Object());

        assertFalse(result);
    }

    @Test
    public void testEquals_ObjectIsInstanceOfRoomButDifferentAttributes_ReturnsFalse() {
        Room room1 = new Room("RoomA", 10);
        Room room2 = new Room("RoomB", 10);

        boolean result = room1.equals(room2);

        assertFalse(result);
    }

    @Test
    public void testEquals_ObjectIsInstanceOfRoomAndSameAttributes_ReturnsTrue() {
        Room room1 = new Room("RoomA", 10);
        Room room2 = new Room("RoomA", 10);

        boolean result = room1.equals(room2);

        assertTrue(result);
    }

}