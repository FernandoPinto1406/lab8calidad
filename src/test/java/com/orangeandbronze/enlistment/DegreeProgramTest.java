package com.orangeandbronze.enlistment;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.*;

public class DegreeProgramTest {

    @Test
    public void testConstructorAndGetters() {
        Collection<Subject> subjects = Arrays.asList(new Subject("Math", 123), new Subject("Science", 321));
        DegreeProgram degreeProgram = new DegreeProgram("BSc", subjects);
        assertEquals("BSc", degreeProgram.toString());
        assertTrue(degreeProgram.containsSubject(new Subject("Math", 132)));
        assertFalse(degreeProgram.containsSubject(new Subject("English", 321)));
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorWithNullSubjects() {
        new DegreeProgram("BSc", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithBlankProgramName() {
        new DegreeProgram("", Collections.emptyList());
    }

    @Test
    public void testEqualsAndHashCode() {
        Collection<Subject> subjects1 = Arrays.asList(new Subject("Math", 123), new Subject("Science", 321));
        Collection<Subject> subjects2 = Arrays.asList(new Subject("Math", 123), new Subject("Science", 321));
        Collection<Subject> subjects3 = Collections.singletonList(new Subject("History", 789));

        DegreeProgram degreeProgram1 = new DegreeProgram("BSc", subjects1);
        DegreeProgram degreeProgram2 = new DegreeProgram("BSc", subjects2);
        DegreeProgram degreeProgram3 = new DegreeProgram("BA", subjects3);

        assertTrue(degreeProgram1.equals(degreeProgram2));
        assertFalse(degreeProgram1.equals(degreeProgram3));
        assertEquals(degreeProgram1.hashCode(), degreeProgram2.hashCode());
    }

    @Test
    public void testEquals_SameObject_ReturnsTrue() {
        Collection<Subject> subjects = new ArrayList<>();
        DegreeProgram program = new DegreeProgram("Program", subjects);

        assertTrue(program.equals(program));
    }

    @Test
    public void testEquals_NullObject_ReturnsFalse() {
        Collection<Subject> subjects = new ArrayList<>();
        DegreeProgram program = new DegreeProgram("Program", subjects);

        assertFalse(program.equals(null));
    }

    @Test
    public void testEquals_DifferentClass_ReturnsFalse() {
        Collection<Subject> subjects = new ArrayList<>();
        DegreeProgram program = new DegreeProgram("Program", subjects);

        assertFalse(program.equals("not a DegreeProgram object"));
    }

    @Test
    public void testEquals_DifferentSubjects_ReturnsFalse() {
        Collection<Subject> subjects1 = new ArrayList<>();
        Collection<Subject> subjects2 = new ArrayList<>();
        subjects1.add(new Subject("Subject1", 3));
        subjects2.add(new Subject("Subject2", 4));

        DegreeProgram program1 = new DegreeProgram("Program", subjects1);
        DegreeProgram program2 = new DegreeProgram("Program", subjects2);

        assertFalse(program1.equals(program2));
    }

    @Test
    public void testEquals_SameSubjectsAndProgramName_ReturnsTrue() {
        Collection<Subject> subjects1 = new ArrayList<>();
        subjects1.add(new Subject("Subject1", 3));
        subjects1.add(new Subject("Subject2", 4));

        Collection<Subject> subjects2 = new ArrayList<>();
        subjects2.add(new Subject("Subject1", 3));
        subjects2.add(new Subject("Subject2", 4));

        DegreeProgram program1 = new DegreeProgram("Program", subjects1);
        DegreeProgram program2 = new DegreeProgram("Program", subjects2);

        assertTrue(program1.equals(program2));
    }
}
