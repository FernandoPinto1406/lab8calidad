package com.orangeandbronze.enlistment;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class ScheduleTest {
    @Test
    public void testConstructorAndGetters() {
        Collection<Subject> subjects = new HashSet<>(Arrays.asList(
                new Subject("Math", 101),
                new Subject("Science", 102)
        ));
        DegreeProgram program = new DegreeProgram("Bachelor of Science", subjects);

        assertEquals("Bachelor of Science", program.toString());
        assertTrue(program.containsSubject(new Subject("Math", 101)));
        assertTrue(program.containsSubject(new Subject("Science", 102)));
        assertFalse(program.containsSubject(new Subject("History", 103)));
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorWithNullSubjects() {
        new DegreeProgram("Bachelor of Arts", null);
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorWithNullDays() {
        new Schedule(null, new Period(830, 900));
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorWithNullPeriod() {
        new Schedule(Days.MTH, null);
    }

    @Test
    public void testCheckOverlaps_SameDaysAndNonOverlappingPeriods() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 1000));
        Schedule schedule2 = new Schedule(Days.MTH, new Period(1100, 1200));
        assertDoesNotThrow(() -> schedule1.checkOverlaps(schedule2));
    }

    @Test
    public void testCheckOverlaps_DifferentDays() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 1000));
        Schedule schedule2 = new Schedule(Days.TF, new Period(830, 1000));
        assertDoesNotThrow(() -> schedule1.checkOverlaps(schedule2));
    }

    @Test
    public void testEquals_DifferentSchedule() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 1000));
        Schedule schedule2 = new Schedule(Days.TF, new Period(830, 1000));
        assertNotEquals(schedule1, schedule2);
    }

    @Test
    public void testGetDays() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 1000));
        assertEquals(Days.MTH, schedule.getDays());
    }

    @Test
    public void testGetPeriod() {
        Period period = new Period(830, 1000);
        Schedule schedule = new Schedule(Days.MTH, period);
        assertEquals(period, schedule.getPeriod());
    }

    @Test
    public void testToString() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));
        assertNotEquals("MTH 0830-0930", schedule.toString());
    }

    @Test
    public void testEquals() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule2 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule3 = new Schedule(Days.TF, new Period(830, 930));

        assertNotEquals(schedule1, schedule2);
        assertNotEquals(schedule1, schedule3);
    }

    @Test
    public void testHashCode() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule2 = new Schedule(Days.MTH, new Period(830, 930));

        assertNotEquals(schedule1.hashCode(), schedule2.hashCode());
    }

    @Test
    public void testEquals_SameClass_ReturnsTrue() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule2 = new Schedule(Days.MTH, new Period(930, 1030));

        assertFalse(schedule1.equals(schedule2));
    }

    @Test
    public void testEquals_WhenObjectIsSameInstance_ReturnsTrue() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));

        boolean result = schedule.equals(schedule);

        assertTrue(result);
    }

    @Test
    public void testEquals_WhenObjectIsNotSameClass_ReturnsFalse() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));
        Object otherObject = new Object();

        boolean result = schedule.equals(otherObject);

        assertFalse(result);
    }

    @Test
    public void testEquals_SameObject_ReturnsTrue() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));

        assertTrue(schedule.equals(schedule));
    }

    @Test
    public void testEquals_NullObject_ReturnsFalse() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));

        assertFalse(schedule.equals(null));
    }

    @Test
    public void testEquals_DifferentClass_ReturnsFalse() {
        Schedule schedule = new Schedule(Days.MTH, new Period(830, 930));

        assertFalse(schedule.equals("not a Schedule object"));
    }

    @Test
    public void testEquals_DifferentDays_ReturnsFalse() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule2 = new Schedule(Days.TF, new Period(830, 930));

        assertFalse(schedule1.equals(schedule2));
    }

    @Test
    public void testEquals_DifferentPeriods_ReturnsFalse() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule2 = new Schedule(Days.MTH, new Period(930, 1030));

        assertFalse(schedule1.equals(schedule2));
    }

    @Test
    public void testEquals_EqualDaysAndPeriods_ReturnsTrue() {
        Schedule schedule1 = new Schedule(Days.MTH, new Period(830, 930));
        Schedule schedule2 = new Schedule(Days.MTH, new Period(830, 930));

        assertFalse(schedule1.equals(schedule2));
    }
}
